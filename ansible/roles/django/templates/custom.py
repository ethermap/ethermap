SITE_URL = 'https://{{ inventory_hostname }}'
DATABASE_URL = '{{ database_url }}'
DEFAULT_FROM_EMAIL = '{{ project_name }} <noreply@{{ inventory_hostname }}>'
