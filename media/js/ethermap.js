var map = undefined;
var layer = undefined;
var timer = undefined;
var cachebust = undefined;
var markers = [];
var adding = false;
var resultsLayer = undefined;

$.fn.extend({
  focusAtEnd: function () {
    return this.each(function() {
      var val = $(this).val();
      $(this).focus().val('').val(val);
    });
  },

  showErrors: function (errors, success) {
    success = success || function () {};

    if ($.isEmptyObject(errors)) {
      return success();
    }

    var form = $(this);
    $.each(errors, function (k, v) {
      var input = form.find('[name=' + k + ']');

      input
        .addClass('is-invalid')
        .removeClass('is-valid')
        .siblings('.invalid-feedback')
          .remove()
        .end()
        .focusAtEnd()
        ;

      // We're inserting each new error, so reverse the list.
      $.each(v.reverse(), function () {
        var div = $('<div class="invalid-feedback"/>').html(this.message);
        input.after(div);
      });
    });

    return this;
  }
});


$.extend({
  feature: function (body_class, callback) {
    if ($('body').hasClass(body_class)) {
      $(function() {
        callback();
      });
    }
  },

  addMarker: function (lat, lng) {
    var button = $('.js-add-marker');

    if (button.length === 0) {
      return;
    }

    $.get(button.data('url'), function (html) {
      var modal = $(html);
      var form = modal.find('form');

      $('#navbar-collapse').collapse('hide');

      form.on('submit', function (e) {
        e.preventDefault();
        modal.find('.js-save-marker').click();
      });

      form.find('[name=lat]').val(lat);
      form.find('[name=lng]').val(lng);
      form.find('[name=title]').val('');
      form.find('[name=description]').val('');

      modal.modal();
      modal.on('shown.bs.modal', function (e) {
        form.find('[name=title]').focusAtEnd();
      });
      modal.on('hidden.bs.modal', function (e) {
        // Reset UI
        $.addingMode(false);
        modal.remove();
      });
    });
  },

  addingMode: function (enable) {
    var button = $('.js-add-marker');

    adding = enable;

    if (adding) {
      button.text(button.data('adding'));
      $('#mapid').css('cursor', 'crosshair');
    } else {
      button.text(button.data('default'));
      $('#mapid').css('cursor', 'grab');
    }
  },

  getIcon: function (icon) {
    return L.icon({
        iconUrl: icons[icon].url
      , iconAnchor: [12,41]
      , popupAnchor: [1,-34]
      , tooltipAnchor: [16,-28]
    });
  },

  sync: function (count) {
    count = count || 1;

    var url = $('#mapid').data('url');

    clearTimeout(timer);

    $.ajax({
        url: url
      , data: {'cachebust': cachebust}
      , dataType: 'json'
    }).always(function (data) {
      // If we had updated data, sync after X seconds.
      if (data.markers) {
        var seconds = 2;
        timer = setTimeout(function () {
          $.sync();
        }, seconds * 1000);
        return;
      }

      // ... otherwise employ exponential backoff in both error and success
      // cases.
      var seconds = Math.min(Math.pow(count + 1, 2), 60 * 5);
      timer = setTimeout(function () {
        $.sync(count + 1);
      }, seconds * 1000);

    }).done(function (data) {
      if (!data.markers) {
        return;
      }

      // Create map if it does not exist
      if (typeof map === 'undefined') {
        // Show/hide controls first, so map size is known, etc.
        $('#mapid').removeClass('d-none');
        $('.js-progress').remove();
        $('.navbar-nav').removeClass('fade');

        map = L.map('mapid', {
          zoom: data.map.zoom,
          layers: [],
        });

        if (data.map.lat && data.map.lng) {
          map.panTo({'lat': data.map.lat, 'lng': data.map.lng});
        } else {
          map.panTo({'lat': '51.490', 'lng': '-0.120'});
          map.locate({setView: true});
        }

        L.control.scale().addTo(map);

        map.on('click', function (e) {
          // Require shift + click if not in "adding" mode.
          if (!adding && !e.originalEvent.shiftKey) {
            return;
          }

          $.addMarker(e.latlng.lat, e.latlng.lng);
        });
      }

      // Set/update Layer if necessary
      if (typeof layer === 'undefined' || layer.options.id != data.map.layer) {
        layer = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
          id: data.map.layer,
          maxZoom: 23,
          tileSize: 512,
          zoomOffset: -1,
          accessToken: data.mapbox_access_token,
          attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>'
        });

        map.eachLayer(function (x) {
          map.removeLayer(x);
        });
        layer.addTo(map);

        // Search
        resultsLayer = new L.LayerGroup().addTo(map);

        var searchControl = new L.esri.Controls.Geosearch().addTo(map);
        searchControl.on('results', function (data){
          resultsLayer.clearLayers();

          var icon = $.getIcon('red');
          $.each(data.results, function (k, v) {
            var marker = L.marker(v.latlng, {'icon': icon});

            marker.on('click', function () {
              $.addMarker(v.latlng.lat, v.latlng.lng);
              resultsLayer.clearLayers();
            });

            resultsLayer.addLayer(marker);
          });
        });
      }

      // Set titles
      $('.navbar-brand').html(data.map.brand);
      document.querySelector('title').innerHTML = data.map.title;

      cachebust = data.cachebust;

      // Remove all existing markers
      $.each(markers, function (k, v) {
        v.remove();
      });

      // Add/save all markers
      $.each(data.markers, function (k, v) {
        var marker = L
          .marker([v.lat, v.lng], {
              'icon': $.getIcon(v.icon)
            , 'title': v.title
          })
          .addTo(map)
          .bindPopup(v.html, {
            minWidth: 200
          })
          ;

        markers.push(marker);
      });

      map.closePopup();
      resultsLayer.clearLayers();
    });
  },
});

$.ajaxSetup({
  beforeSend: function(xhr, settings) {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
        // Only send the token to relative URLs i.e. locally.
        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
    }
  }
});

$.feature('f_maps_view', function () {
  $('.js-copy-to-clipboard').on('click', function (e) {
    var modal = $(this).parents('#share-modal');

    modal.find('.js-uri').select();

    document.execCommand('copy');
  });

  $('.js-edit').on('click', function (e) {
    e.preventDefault();

    var elem = $(this);

    $.get(elem.data('url'), function (html) {
      var modal = $(html);
      var form = modal.find('form');

      $('#navbar-collapse').collapse('hide');

      form.on('submit', function (e) {
        e.preventDefault();
        modal.find('.js-authenticate').click();
      });

      modal.modal();
      modal.on('shown.bs.modal', function (e) {
        form.find('[name=password]').focusAtEnd();
      });
      modal.on('hidden.bs.modal', function (e) {
        modal.remove();
      });
    });
  });

  $(document).on('click', '.js-authenticate', function (e) {
    e.preventDefault();

    var elem = $(this);
    var form = elem.parents('.modal').find('form');


    $.post(elem.data('url'), form.serialize(), function (errors) {
      form.showErrors(errors, function () {
        window.location.reload();
      });
    })
  });

  // Remove validation errors text upon changing a field
  $(document).on('cchange keyup keydown paste click', 'input', function () {
    $(this).on('change keyup paste click', function () {
      $(this).removeClass('is-invalid');
    });
  });

  // Handle ESC
  $(document).on('keydown', '.modal input, .modal textarea', function (e) {
    if (e.keyCode != 27) {
      return true;
    }

    e.preventDefault();

    $(this).parents('.modal').modal().hide();
  });
});

$.feature('f_maps_edit', function () {
  $('.js-settings').on('click', function (e) {
    e.preventDefault();

    var elem = $(this);

    $.get(elem.data('url'), function (html) {
      var modal = $(html);
      var form = modal.find('form');

      modal.modal();
      modal.on('shown.bs.modal', function (e) {
        form.find('[name=title]').focusAtEnd();

        form.on('submit', function (e) {
          e.preventDefault();
          modal.find('.js-save-settings').click();
        });
      });
      modal.on('hidden.bs.modal', function (e) {
        modal.remove();
      });
    });
  });

  $(document).on('click', '.js-password', function (e) {
    e.preventDefault();

    var elem = $(this);

    $.get(elem.data('url'), function (html) {
      var modal = $(html);
      var form = modal.find('form');

      modal.modal();
      modal.on('shown.bs.modal', function (e) {
        form.on('submit', function (e) {
          e.preventDefault();
          modal.find('.js-set-password').click();
        });
      });
      modal.on('hidden.bs.modal', function (e) {
        modal.remove();
      });
    });
  });

  $('.js-add-marker').on('click', function (e) {
    e.preventDefault();

    $.addingMode(true);
  });

  $(document).on('click', '.js-save-marker, .js-save-settings, .js-set-password', function (e) {
    e.preventDefault();

    var elem = $(this);
    var modal = elem.parents('.modal');
    var form = modal.find('form');

    elem.text(elem.data('saving'));

    $.post(elem.data('url'), form.serialize(), function (errors) {
      elem.text(elem.data('original'));

      form.showErrors(errors, function () {
        modal.modal('hide');
        $.sync();
      });
    });
  });

  $('#mapid').on('click', '.js-edit-marker', function (e) {
    e.preventDefault();

    var elem = $(this);

    $.get(elem.data('url'), function (html) {
      var modal = $(html);
      var form = modal.find('form');

      modal.modal();
      modal.on('shown.bs.modal', function (e) {
        form.find('[name=title]').focusAtEnd();

        form.on('submit', function (e) {
          e.preventDefault();
          modal.find('.js-save-marker').click();
        });
      });
      modal.on('hidden.bs.modal', function (e) {
        modal.remove();
      });
    });
  });

  $(document).on('click', '.js-delete-marker', function (e) {
    e.preventDefault();

    var elem = $(this);
    var modal = elem.parents('.modal');

    if (confirm(elem.data('confirm'))) {
      $.post(elem.data('url'), function () {
        modal.modal('hide');
        $.sync();
      });
    }
  });

  $('.js-search').on('click', function (e) {
    e.preventDefault();

    $('.geocoder-control.leaflet-control').click()
  });

  $('.js-save-center').on('click', function (e) {
      e.preventDefault();

      var elem = $(this);

      elem.text(elem.data('saving'));

      $.post(elem.data('url'), {
          'lat': map.getCenter().lat
        , 'lng': map.getCenter().lng
        , 'zoom': map.getZoom()
      }, function () {
        elem.text(elem.data('saved'));

        setTimeout(function () {
          elem.text(elem.data('default'));
        }, 1000);
      })
  });

  Mousetrap.bind('s', function () {
    $('.js-search').click();
    return false;
  });

  Mousetrap.bind('S', function () {
    $('#share-modal').modal().show();
    return false;
  });

  Mousetrap.bind('c', function () {
    $('.js-save-center').click();
    return false;
  });

  Mousetrap.bind('x', function () {
    $('.js-settings').click();
    return false;
  });

  Mousetrap.bind('a', function () {
    if (adding) {
      $.addingMode(false);
    } else {
      $('.js-add-marker').click();
    }
    return false;
  });

  Mousetrap.bind('?', function () {
    $('#about-modal').modal().show();
    return false;
  });

  Mousetrap.bind('e', function () {
    $('.js-edit').click();
    return false;
  });

  Mousetrap.bind('escape', function () {
    if (adding) {
      $.addingMode(false);
      return false;
    }
  });
});

$.feature('f_maps_login', function () {
  var modal = $('#login-modal');
  var form = modal.find('form');

  modal.modal();

  modal.on('shown.bs.modal', function (e) {
    form.find('[name=password]').focusAtEnd();
  });
  form.on('submit', function (e) {
    e.preventDefault();
    modal.find('.js-authenticate').click();
  });
});
