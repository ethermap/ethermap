from django.conf import settings
from django.conf.urls import include, url
from django.views.static import serve

urlpatterns = (
    url("", include("ethermap.maps.urls", namespace="maps")),
    url(r"", include("ethermap.static.urls", namespace="static")),
)

if settings.DEBUG:
    urlpatterns += (
        url(
            r"^(?P<path>favicon.ico)$",
            serve,
            {"document_root": settings.STATIC_ROOT},
        ),
        url(
            r"^storage/(?P<path>.*)$",
            serve,
            {"document_root": settings.MEDIA_ROOT},
        ),
    )
