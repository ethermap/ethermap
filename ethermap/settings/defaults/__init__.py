import os
import copy

from os.path import dirname, abspath

from django.utils.log import DEFAULT_LOGGING

from .apps import *
from .setup_warnings import *

DEBUG = False
BASE_DIR = dirname(dirname(dirname(dirname(abspath(__file__)))))
ALLOWED_HOSTS = ("*",)

SECRET_KEY = "overriden-in-production"

DATABASES = {}
DATABASE_URL = "postgresql://ethermap:ethermap@127.0.0.1:5432/ethermap"

MIDDLEWARE = (
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.middleware.security.SecurityMiddleware",
)

ROOT_URLCONF = "ethermap.urls"
WSGI_APPLICATION = "ethermap.wsgi.application"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.messages.context_processors.messages",
                "ethermap.utils.context_processors.settings_context",
            ],
            "builtins": [
                "django.contrib.staticfiles.templatetags.staticfiles",
                "django_markdown2.templatetags.md2",
                "ethermap.utils.templatetags.text",
            ],
        },
    }
]

USE_TZ = False
TIME_ZONE = "UTC"
USE_I18N = False
USE_L10N = False
LANGUAGE_CODE = "en-gb"
DATETIME_FORMAT = "r"  # RFC 2822

STATIC_URL = "/static/"
STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATICFILES_DIRS = (os.path.join(BASE_DIR, "media"),)
STATICFILES_STORAGE = (
    "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"
)

MEDIA_URL = "/storage/"
MEDIA_ROOT = "overriden-in-production"
DEFAULT_FILE_STORAGE = "django.core.files.storage.FileSystemStorage"

SITE_URL = "overriden-in-production"
DEFAULT_FROM_EMAIL = "overriden-in-production"

CSRF_COOKIE_SECURE = True
SESSION_COOKIE_AGE = 86400 * 365 * 10
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = True
SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.memcached.MemcachedCache",
        "LOCATION": "127.0.0.1:11211",
        "KEY_PREFIX": "ethermap",
    }
}

# Always log to the console, even in production (ie. gunicorn)
LOGGING = copy.deepcopy(DEFAULT_LOGGING)
LOGGING["handlers"]["console"]["filters"] = []

MAPBOX_ACCESS_TOKEN = "pk.eyJ1IjoiZXRoZXJtYXAiLCJhIjoiY2tzcTM5em8wMDk4MzJubWRmdzRsZ2hsMCJ9.AEJtTOO4LXdi8Vt-opFQfg"

SESSION_ENGINE = "django.contrib.sessions.backends.cached_db"

ICONS = (
    ("blue", "Blue"),
    ("black", "Black"),
    ("green", "Green"),
    ("grey", "Grey"),
    ("orange", "Orange"),
    ("red", "Red"),
    ("violet", "Violet"),
    ("yellow", "Yellow"),
)
