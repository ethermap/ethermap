INSTALLED_APPS = (
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.staticfiles",
    "django_markdown2",
    "ethermap.maps",
    "ethermap.maps.maps_markers",
    "ethermap.static",
    "ethermap.utils",
)
