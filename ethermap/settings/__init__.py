import sys
import dj_database_url

from .defaults import *

if sys.argv[1:2] == ["test"]:
    from .roles.test import *
else:
    from .role import *

    try:
        from .custom import *
    except ImportError:
        pass

DATABASES["default"] = dj_database_url.parse(DATABASE_URL)
DATABASES["default"]["ATOMIC_REQUESTS"] = True
