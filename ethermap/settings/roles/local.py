import os

from django.utils.log import DEFAULT_LOGGING as LOGGING

BASE_DIR = os.path.dirname(
    os.path.dirname(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    )
)

DEBUG = True

SITE_URL = "http://127.0.0.1:8000"
MEDIA_ROOT = os.path.join(BASE_DIR, "storage")

DATABASE_URL = "sqlite:///{}".format(os.path.join(BASE_DIR, "db.sqlite"))

CSRF_COOKIE_SECURE = False
SESSION_COOKIE_SECURE = False

CACHES = {
    "default": {"BACKEND": "django.core.cache.backends.locmem.LocMemCache"}
}
