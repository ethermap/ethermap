from .local import *

MEDIA_ROOT = "/tmp"
DATABASE_URL = "sqlite://:memory:"
STATICFILES_STORAGE = "django.contrib.staticfiles.storage.StaticFilesStorage"
