from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from django.views.decorators.http import require_POST

from ..utils import get_map

from .forms import CreateMarkerForm, EditMarkerForm


def xhr_add(request, map_slug):
    map = get_map(request, slug=map_slug)

    if request.method == "POST":
        form = CreateMarkerForm(request.POST)

        if form.is_valid():
            marker = form.save(commit=False)
            marker.map = map
            marker.save()
            map.markers_changed()

        return JsonResponse(form.errors.get_json_data())

    form = CreateMarkerForm()

    return render(
        request, "maps/xhr_add_edit_marker.html", {"map": map, "form": form}
    )


def xhr_edit(request, map_slug, slug):
    map = get_map(request, slug=map_slug)
    marker = get_object_or_404(map.markers, slug=slug)

    if request.method == "POST":
        form = EditMarkerForm(request.POST, instance=marker)

        if form.is_valid():
            form.save()
            map.markers_changed()

        return JsonResponse(form.errors.get_json_data())

    form = EditMarkerForm(instance=marker)

    return render(
        request,
        "maps/xhr_add_edit_marker.html",
        {"map": map, "form": form, "marker": marker},
    )


@require_POST
def xhr_delete(request, map_slug, slug):
    map = get_map(request, slug=map_slug)
    marker = get_object_or_404(map.markers, slug=slug)

    marker.delete()

    map.markers_changed()

    return JsonResponse({})
