from django.urls import path

from . import views

app_name = "maps_markers"

urlpatterns = (
    path("xhr/<slug:map_slug>/add", views.xhr_add, name="xhr-add"),
    path(
        "xhr/<slug:map_slug>/<slug:slug>/edit", views.xhr_edit, name="xhr-edit"
    ),
    path(
        "xhr/<slug:map_slug>/<slug:slug>/delete",
        views.xhr_delete,
        name="xhr-delete",
    ),
)
