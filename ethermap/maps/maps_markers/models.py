from django.db import models
from django.conf import settings
from django.utils.crypto import get_random_string


class Marker(models.Model):
    map = models.ForeignKey(
        "maps.Map", on_delete=models.CASCADE, related_name="markers"
    )

    slug = models.CharField(max_length=12, default=get_random_string)

    lat = models.CharField(max_length=255)
    lng = models.CharField(max_length=255)

    icon = models.CharField(
        max_length=64, choices=settings.ICONS, default=settings.ICONS[0][0]
    )

    title = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return "pk={0.pk} map={0.map_id} title={0.title!r}".format(self)

    class Meta:
        unique_together = (("map", "slug"),)
