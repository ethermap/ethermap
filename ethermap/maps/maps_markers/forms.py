from django import forms

from .models import Marker


class CreateMarkerForm(forms.ModelForm):
    class Meta:
        model = Marker
        fields = ("lat", "lng", "title", "description", "icon")


class EditMarkerForm(forms.ModelForm):
    class Meta:
        model = Marker
        fields = ("title", "description", "icon")
