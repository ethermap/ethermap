from django.urls import path, include

from . import views

app_name = "maps"

urlpatterns = (
    path("", include("ethermap.maps.maps_markers.urls", namespace="markers")),
    path("", views.create, name="create"),
    path("m/<slug:slug>", views.view, name="view"),
    path("m/<slug:slug>.json", views.export, name="export"),
    path("xhr/<slug:slug>", views.xhr_data, name="xhr-data"),
    path("xhr/<slug:slug>/settings", views.xhr_settings, name="xhr-settings"),
    path("xhr/<slug:slug>/password", views.xhr_password, name="xhr-password"),
    path(
        "xhr/<slug:slug>/save-center",
        views.xhr_save_center,
        name="xhr-save-center",
    ),
    path(
        "xhr/<slug:slug>/authenticate",
        views.xhr_authenticate,
        name="xhr-authenticate",
    ),
)
