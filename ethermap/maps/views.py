from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.template.loader import render_to_string
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.http import require_POST

from .forms import (
    CreateForm,
    AuthenticationForm,
    SettingsForm,
    PasswordForm,
    SaveCenterForm,
)
from .utils import get_map, can_edit
from .models import Map


def create(request):
    if request.method == "POST":
        form = CreateForm(request.POST)

        if form.is_valid():
            map = form.save()

            return redirect(map)
    else:
        form = CreateForm()

    return render(request, "maps/create.html", {"form": form})


@ensure_csrf_cookie
def view(request, slug):
    map = get_object_or_404(Map, slug=slug)
    form = AuthenticationForm(map)
    template = "maps/edit.html"

    try:
        template = "maps/edit.html"
        get_map(request, slug=slug, map=map)
    except PermissionDenied:
        template = "maps/view.html"

        if map.private:
            template = "maps/login.html"

    return render(request, template, {"map": map, "form": form})


def xhr_data(request, slug):
    map = get_map(request, slug=slug, login_required=False)

    # Don't update the client if there have been no changes
    if request.GET.get("cachebust") == str(map.cachebust):
        return JsonResponse({})

    show_edit_controls = can_edit(request, map)

    markers = [
        {
            "lat": x.lat,
            "lng": x.lng,
            "icon": x.icon,
            "title": x.title,
            "html": render_to_string(
                "maps/include_marker.html",
                {"marker": x, "show_edit_controls": show_edit_controls},
            ),
        }
        for x in map.markers.all()
    ]

    return JsonResponse(
        {
            "map": {
                "lat": map.lat,
                "lng": map.lng,
                "zoom": map.zoom,
                "layer": map.layer,
                "brand": render_to_string(
                    "maps/include_brand.html", {"map": map}
                ),
                "title": render_to_string(
                    "maps/include_title.html", {"map": map}
                ),
            },
            "markers": markers,
            "cachebust": map.cachebust,
            "mapbox_access_token": settings.MAPBOX_ACCESS_TOKEN,
        }
    )


def export(request, slug):
    map = get_map(request, slug=slug, login_required=False)

    features = [
        {
            "type": "Feature",
            "properties": {"name": x.title, "description": x.description},
            "geometry": {
                "type": "Point",
                "coordinates": [float(x.lng), float(x.lat)],
            },
            "_ethermap": {"icon": x.icon},
        }
        for x in map.markers.all()
    ]

    return JsonResponse(
        {
            "type": "FeatureCollection",
            "features": features,
            "_ethermap": {
                "lat": map.lat,
                "lng": map.lng,
                "zoom": map.zoom,
                "layer": map.layer,
                "title": map.title,
            },
        },
        json_dumps_params={"indent": 2},
    )


def xhr_authenticate(request, slug):
    map = get_object_or_404(Map, slug=slug)

    if request.method == "POST":
        form = AuthenticationForm(map, request.POST)

        if form.is_valid():
            form.save(request)

        return JsonResponse(form.errors.get_json_data())

    form = AuthenticationForm(map)

    return render(
        request, "maps/xhr_authenticate.html", {"map": map, "form": form}
    )


def xhr_settings(request, slug):
    map = get_map(request, slug=slug)

    if request.method == "POST":
        form = SettingsForm(request.POST, instance=map)

        if form.is_valid():
            form.save()

        return JsonResponse(form.errors.get_json_data())

    form = SettingsForm(instance=map)

    return render(
        request, "maps/xhr_settings.html", {"map": map, "form": form}
    )


def xhr_password(request, slug):
    map = get_map(request, slug=slug)

    if map.password:
        raise PermissionDenied("Cannot edit existing password.")

    if request.method == "POST":
        form = PasswordForm(request.POST, instance=map)

        if form.is_valid():
            form.save(request)

        return JsonResponse(form.errors.get_json_data())

    form = PasswordForm(instance=map)

    return render(
        request, "maps/xhr_password.html", {"map": map, "form": form}
    )


@require_POST
def xhr_save_center(request, slug):
    map = get_map(request, slug=slug)

    form = SaveCenterForm(request.POST, instance=map)

    if form.is_valid():
        form.save()

    return JsonResponse(form.errors.get_json_data())
