from django.shortcuts import get_object_or_404
from django.core.exceptions import PermissionDenied

from .models import Map


def get_map(request, slug, login_required=True, map=None):
    if map is None:
        map = get_object_or_404(Map, slug=slug)

    if not can_edit(request, map, login_required):
        raise PermissionDenied()

    return map


def can_edit(request, map, login_required=True):
    if map.private:
        if request.session.get(map.slug, "") != map.password:
            return False
    else:
        if (
            login_required
            and request.session.get(map.slug, "") != map.password
        ):
            return False

    return True
