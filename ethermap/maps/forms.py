import emoji
import slugify

from django import forms
from django.contrib.auth.hashers import check_password, make_password

from .models import Map


class CreateForm(forms.ModelForm):
    class Meta:
        model = Map
        fields = ("title",)

    def save(self):
        slug = slugify.slugify(
            emoji.demojize(self.cleaned_data["title"].strip()),
            max_length=Map._meta.get_field("slug").max_length,
        )

        if slug:
            try:
                return Map.objects.get(slug=slug)
            except Map.DoesNotExist:
                pass

        instance = super().save(commit=False)
        if slug:
            instance.slug = slug
        instance.save()

        return instance


class SettingsForm(forms.ModelForm):
    class Meta:
        model = Map
        fields = ("title", "layer")

    def save(self):
        instance = super().save()
        instance.markers_changed()
        return instance


class PasswordForm(forms.ModelForm):
    password1 = forms.CharField()
    password2 = forms.CharField()

    class Meta:
        model = Map
        fields = ("private",)

    def clean(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        if password1 != password2:
            self.add_error("password2", "Passwords do not match")

        return self.cleaned_data

    def save(self, request):
        instance = super().save(commit=False)

        hashed = make_password(self.cleaned_data["password1"])
        instance.password = hashed

        # Grant the user access
        request.session[instance.slug] = hashed

        instance.save()

        instance.markers_changed()

        return instance


class SaveCenterForm(forms.ModelForm):
    class Meta:
        model = Map
        fields = ("lat", "lng", "zoom")


class AuthenticationForm(forms.Form):
    password = forms.CharField()

    def __init__(self, map, *args, **kwargs):
        self.map = map

        super().__init__(*args, **kwargs)

    def clean_password(self):
        val = self.cleaned_data["password"]

        if not check_password(val, self.map.password):
            raise forms.ValidationError("Invalid password")

        return val

    def save(self, request):
        # Store the password hash in the session for authentication
        request.session[self.map.slug] = self.map.password
