import functools

from django.db import models
from django.urls import reverse
from django.utils.crypto import get_random_string

MAP_LAYERS = (
    ("mapbox/streets-v11", "Colour"),
    ("mapbox/light-v9", "Greyscale"),
)


class Map(models.Model):
    title = models.CharField(max_length=255, blank=True)

    slug = models.CharField(
        unique=True,
        default=functools.partial(
            get_random_string, 20, "abcdefghjkmnpqrstwxyz3479"
        ),
        max_length=64,
    )

    layer = models.CharField(
        choices=MAP_LAYERS, default=MAP_LAYERS[0][0], max_length=128,
    )

    password = models.CharField(max_length=128, blank=True)

    # If true and a password is set, require the password to view the map.
    private = models.BooleanField(default=False, blank=True)

    lat = models.CharField(max_length=255)
    lng = models.CharField(max_length=255)
    zoom = models.IntegerField(default=14)

    cachebust = models.CharField(max_length=12, default=get_random_string)

    def __str__(self):
        return "pk={0.pk}".format(self)

    def get_absolute_url(self):
        return reverse("maps:view", args=(self.slug,))

    def markers_changed(self):
        self.cachebust = get_random_string()

        if self.markers.count() != 1:
            self.save(update_fields=("cachebust",))
            return

        marker = self.markers.get()

        self.lat = marker.lat
        self.lng = marker.lng
        self.zoom = Map._meta.get_field("zoom").default

        self.save()
