import re

from django import template

re_url = re.compile(r"\b(?<!\()(https?://\[?\S+)", re.IGNORECASE)
register = template.Library()


@register.filter
def preprocess_markdown(val):
    def fn(m):
        url = m.group(1)

        for repl in "\\*_":
            url = url.replace(repl, r"\{}".format(repl))

        return "[{0}]({0})".format(url)

    return re_url.sub(fn, val)
